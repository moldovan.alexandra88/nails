export function Banner() {
  return (
    <section className="relative h-[500px] w-full">
      <img
        src="https://i.pinimg.com/564x/d3/2c/73/d32c730d62c6e0b30949c5b49fd10bec.jpg"
        alt="Image"
        className="w-full h-full object-cover object-center"
      />
    </section>
  );
}
