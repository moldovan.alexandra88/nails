'use client'

import React from "react";
import { useState } from 'react';

export default function Header() {

      
  const [isMenuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
  };

  return (
    <header className="sticky top-0 z-30">
      <nav
        className="flex w-full items-center justify-between bg-black py-2 text-neutral-600 shadow-lg md:flex-wrap md:justify-start"
        data-te-navbar-ref
      >
        <div className="flex w-full flex-wrap items-center justify-between px-3">
          <div className="flex items-center">
            <button
              className="border-0 bg-transparent px-2 text-xl leading-none transition-shadow duration-150 ease-in-out hover:text-yellow-500 focus:text-yellow-500 lg:hidden"
              type="button"
              onClick={toggleMenu}
            >
              <span className="[&>svg]:w-5">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth="1.5"
                  stroke="currentColor"
                  className="h-7 w-7"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                  />
                </svg>
              </span>
            </button>
          </div>

          <div
            className={`!visible ${isMenuOpen ? 'block' : 'hidden'} lg:!flex lg:basis-auto grow basis-[100%] items-center`}
            id="navbarSupportedContentY"
            data-te-collapse-item
          >
            <ul className="mr-auto flex flex-col lg:flex-row" data-te-navbar-nav-ref>
              {/* Your navigation links here */}
              <li className="mb-4 lg:mb-0 lg:pr-2" data-te-nav-item-ref>
                <a
                  href="#home"
                  className="block text-yellow-500 font-thin font-sans transition duration-150 ease-in-ou"
                  data-te-nav-link-ref
                  data-te-ripple-init
                  data-te-ripple-color="light"
                >
                  NAILS by Penelope
                </a>
              </li>
            </ul>
            <ul className="ml-auto flex flex-col lg:flex-row" data-te-navbar-nav-ref>
              {/* Your navigation links here */}
              <li className="mb-4 lg:mb-0 lg:pr-2" data-te-nav-item-ref>
                <a
                  href="#home"
                  className="block text-warning-50 uppercase text-base transition duration-150 ease-in-out hover:text-yellow-500 focus:text-yellow-500 lg:p-2 [&.active]:text-black/90"
                  data-te-nav-link-ref
                  data-te-ripple-init
                  data-te-ripple-color="light"
                >
                  Home
                </a>
              </li>
              <li className="mb-4 lg:mb-0 lg:pr-2" data-te-nav-item-ref>
                <a
                  href="#about"
                  className="block text-warning-50 uppercase text-base transition duration-150 ease-in-out hover:text-yellow-500 focus:text-yellow-500 lg:p-2 [&.active]:text-black/90"
                  data-te-nav-link-ref
                  data-te-ripple-init
                  data-te-ripple-color="light"
                >
                  About Us
                </a>
              </li>
              <li className="mb-2 lg:mb-0 lg:pr-2" data-te-nav-item-ref>
                <a
                  href="#services"
                  className="block text-warning-50 uppercase text-base transition duration-150 ease-in-out hover:text-yellow-500 focus:text-yellow-500 disabled:text-black/30 dark:hover:text-white dark:focus:text-white lg:p-2 [&.active]:text-black/90"
                  data-te-nav-link-ref
                  data-te-ripple-init
                  data-te-ripple-color="light"
                >
                  Services
                </a>
              </li>
              <li className="mb-2 lg:mb-0 lg:pr-2" data-te-nav-item-ref>
                <a
                  href="#testimonials"
                  className="block text-warning-50 uppercase text-base transition duration-150 ease-in-out hover:text-yellow-500 focus:text-yellow-500 disabled:text-black/30 dark:hover:text-white dark:focus:text-white lg:p-2 [&.active]:text-black/90"
                  data-te-nav-link-ref
                  data-te-ripple-init
                  data-te-ripple-color="light"
                >
                  Testimonials
                </a>
              </li>
              <li className="mb-2 lg:mb-0 lg:pr-2" data-te-nav-item-ref>
                <a
                  href="#contact"
                  className="block text-warning-50 uppercase text-base transition duration-150 ease-in-out hover:text-yellow-500 focus:text-yellow-500 disabled:text-black/30 dark:hover:text-white dark:focus:text-white lg:p-2 [&.active]:text-black/90"
                  data-te-nav-link-ref
                  data-te-ripple-init
                  data-te-ripple-color="light"
                >
                Contact
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
}


