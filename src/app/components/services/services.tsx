import React from "react";


export function Services() {
  return (
    <section className="container my-24 mx-auto md:px-6" id="services">
      <section className="mb-32">
        <h2 className="mb-12 text-center text-3xl font-bold">Services</h2>

        <div className="grid gap-6 lg:grid-cols-3">
          <div
            className="zoom relative overflow-hidden rounded-lg bg-cover bg-no-repeat shadow-lg dark:shadow-black/20 bg-[50%]"
            data-te-ripple-init
            data-te-ripple-color="dark"
          >
            <img
              src="https://i.pinimg.com/550x/e9/1c/b2/e91cb2256c0183bc3cdf883a68b91ce3.jpg"
              className="w-full align-middle transition duration-300 ease-linear"
            />
            <a href="#!">
              <div className="absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed bg-[hsla(0,0%,0%,0.3)]">
                <div className="flex h-full items-end justify-end">
                  <h5 className="m-6 text-lg font-bold text-white bg-yellow-600 border-2 rounded-md px-4 py-1.5 uppercase transition-all duration-300 ease-in-out hover:bg-yellow-700 hover:text-white">
                    Gel
                  </h5>
                </div>
              </div>
              <div>
                <div className="mask absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100 bg-[hsla(0,0%,99.2%,0.15)]"></div>
              </div>
            </a>
          </div>

          <div
            className="zoom relative overflow-hidden rounded-lg bg-cover bg-no-repeat shadow-lg dark:shadow-black/20 bg-[50%]"
            data-te-ripple-init
            data-te-ripple-color="dark"
          >
            <img
              src="https://blog.cupio.ro/wp-content/uploads/2022/11/unghii-verzi6-1024x1024.jpg"
              className="w-full align-middle transition duration-300 ease-linear"
            />
            <a href="#!">
              <div className="absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed bg-[hsla(0,0%,0%,0.3)]">
                <div className="flex h-full items-end justify-end">
                  <h5 className="m-6 text-lg font-bold text-white bg-yellow-600 border-2 rounded-md px-4 py-1.5 uppercase transition-all duration-300 ease-in-out hover:bg-yellow-700 hover:text-white">
                   Acrylic
                  </h5>
                </div>
              </div>
              <div>
                <div className="mask absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100 bg-[hsla(0,0%,99.2%,0.15)]"></div>
              </div>
            </a>
          </div>

          <div
            className="zoom relative overflow-hidden rounded-lg bg-cover bg-no-repeat shadow-lg dark:shadow-black/20 bg-[50%]"
            data-te-ripple-init
            data-te-ripple-color="dark"
          >
            <img
              src="https://www.karmanailwraps.com/cdn/shop/products/20220603_103101_1024x1024.jpg?v=1667181511"
              className="w-full align-middle transition duration-300 ease-linear"
            />
            <a href="#!">
              <div className="absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed bg-[hsla(0,0%,0%,0.3)]">
                <div className="flex h-full items-end justify-end">
                  <h5 className="m-6 text-lg font-bold text-white bg-yellow-600 border-2 rounded-md px-4 py-1.5 uppercase transition-all duration-300 ease-in-out hover:bg-yellow-700 hover:text-white">
                    French
                  </h5>
                </div>
              </div>
              <div>
                <div className="mask absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100 bg-[hsla(0,0%,99.2%,0.15)]"></div>
              </div>
            </a>
          </div>

          <div
            className="zoom relative overflow-hidden rounded-lg bg-cover bg-no-repeat shadow-lg dark:shadow-black/20 bg-[50%]"
            data-te-ripple-init
            data-te-ripple-color="dark"
          >
            <img
              src="https://viatapetocuri.ro/wp-content/uploads/2022/09/Cute-Fall-Nails-44-1-768x772-1.png"
              className="w-full align-middle transition duration-300 ease-linear"
            />
            <a href="#!">
              <div className="absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed bg-[hsla(0,0%,0%,0.3)]">
                <div className="flex h-full items-end justify-end">
                  <h5 className="m-6 text-lg font-bold text-white bg-yellow-600 border-2 rounded-md px-4 py-1.5 uppercase transition-all duration-300 ease-in-out hover:bg-yellow-700 hover:text-white">
                    Silk
                  </h5>
                </div>
              </div>
              <div>
                <div className="mask absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100 bg-[hsla(0,0%,99.2%,0.15)]"></div>
              </div>
            </a>
          </div>

          <div
            className="zoom relative overflow-hidden rounded-lg bg-cover bg-no-repeat shadow-lg dark:shadow-black/20 bg-[50%]"
            data-te-ripple-init
            data-te-ripple-color="dark"
          >
            <img
              src="https://restaurantcarol.ro/1416_imgs-uploads/Diamant-fals-unghii-colorate-amestecate-pre-modele_1.jpg"
              className="w-full align-middle transition duration-300 ease-linear"
            />
            <a href="#!">
              <div className="absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed bg-[hsla(0,0%,0%,0.3)]">
                <div className="flex h-full items-end justify-end">
                  <h5 className="m-6 text-lg font-bold text-white bg-yellow-600 border-2 rounded-md px-4 py-1.5 uppercase transition-all duration-300 ease-in-out hover:bg-yellow-700 hover:text-white">
                    Shellac
                  </h5>
                </div>
              </div>
              <div>
                <div className="mask absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100 bg-[hsla(0,0%,99.2%,0.15)]"></div>
              </div>
            </a>
          </div>

          <div
            className="zoom relative overflow-hidden rounded-lg bg-cover bg-no-repeat shadow-lg dark:shadow-black/20 bg-[50%]"
            data-te-ripple-init
            data-te-ripple-color="dark"
          >
            <img
              src="https://globalfashion.md/images/blog/articles/3hNK390bn3EBquY8DW7HVPgXstohoy1PJCB8JOhk.png"
              className="w-full align-middle transition duration-300 ease-linear"
            />
            <a href="#!">
              <div className="absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed bg-[hsla(0,0%,0%,0.3)]">
                <div className="flex h-full items-end justify-end">
                  <h5 className="m-6 text-lg font-bold text-white bg-yellow-600 border-2 rounded-md px-4 py-1.5 uppercase transition-all duration-300 ease-in-out hover:bg-yellow-700 hover:text-white">
                    Nail Art
                  </h5>
                </div>
              </div>
              <div>
                <div className="mask absolute top-0 right-0 bottom-0 left-0 h-full w-full overflow-hidden bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-100 bg-[hsla(0,0%,99.2%,0.15)]"></div>
              </div>
            </a>
          </div>
        </div>
      </section>
    </section>
  );
}
