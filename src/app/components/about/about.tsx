import React from "react";

export function About() {
  return (
<div className="container my-24 mx-auto md:px-6">
  <section className="mb-32">
    <div className="flex flex-wrap">
      <div className="mb-12 w-full shrink-0 grow-0 basis-auto lg:mb-0 lg:w-5/12">
        <div className="flex lg:py-12">
          <img src="https://atic.md/wp-content/uploads/2023/01/Modnye-tendencii-manikjura-na-2023-god-foto57-1024x1024.jpg"
            className="z-[10] w-full rounded-lg shadow-lg dark:shadow-black/20 lg:ml-[50px]" alt="image" />
        </div>
      </div>

      <div className="w-full shrink-0 grow-0 basis-auto lg:w-7/12">
        <div
          className="flex h-full items-center rounded-lg p-6 text-center text-black lg:pl-12 lg:text-left">
          <div className="lg:pl-12">
            <h2 className="mb-12 pb-4 text-center text-3xl font-bold">About us</h2>
            <p className="mb-8 pb-2 lg:pb-0">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit.
              Maxime, sint, repellat vel quo quisquam accusamus in qui at
              ipsa enim quibusdam illo laboriosam omnis. Labore itaque illum
              distinctio eum neque!
            </p>

            <div className="mx-auto mb-8 flex flex-col md:flex-row md:justify-around xl:justify-start">
              <p className="mx-auto mb-4 text-yellow-600 flex items-center md:mx-0 md:mb-2 lg:mb-0 xl:mr-20">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2"
                  stroke="currentColor" className="mr-2 h-5 w-5">
                  <path stroke-linecap="round" stroke-linejoin="round"
                    d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Best team
              </p>

              <p className="mx-auto mb-4 text-yellow-600 flex items-center md:mx-0 md:mb-2 lg:mb-0 xl:mr-20">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2"
                  stroke="currentColor" className="mr-2 h-5 w-5">
                  <path stroke-linecap="round" stroke-linejoin="round"
                    d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Best quality
              </p>

              <p className="mx-auto mb-2 text-yellow-600 flex items-center md:mx-0 lg:mb-0">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="2"
                  stroke="currentColor" className="mr-2 h-5 w-5">
                  <path stroke-linecap="round" stroke-linejoin="round"
                    d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
                Best experience
              </p>
            </div>

            <p>
              Duis sagittis, turpis in ullamcorper venenatis, ligula nibh
              porta dui, sit amet rutrum enim massa in ante. Curabitur in
              justo at lorem laoreet ultricies. Nunc ligula felis, sagittis
              eget nisi vitae, sodales vestibulum purus. Vestibulum nibh
              ipsum, rhoncus vel sagittis nec, placerat vel justo. Duis
              faucibus sapien eget tortor finibus, a eleifend lectus dictum.
              Cras tempor convallis magna id rhoncus. Suspendisse potenti.
              Nam mattis faucibus imperdiet. Proin tempor lorem at neque
              tempus aliquet. Phasellus at ex volutpat, varius arcu id,
              aliquam lectus. Vestibulum mattis felis quis ex pharetra
              luctus. Etiam luctus sagittis massa, sed iaculis est vehicula
              ut.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
  );
}

 