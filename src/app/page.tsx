import { ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {Banner} from "@/app/components/banner/banner";
import {About} from "@/app/components/about/about";
import {Services } from "@/app/components/services/services";
import {Testimonials } from "@/app/components/testimonials/testimonials";
import {Contact} from "@/app/components/contact/contact";

export default function Home() {
  return (
   <div className="min-h-[100vh]">
    <Banner/>
    <About/>
    <Services/>
    <Testimonials/>
    <Contact/>
    <ToastContainer />
   </div>
  )
}
